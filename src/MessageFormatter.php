<?php

namespace Archaic\Log;

/** Format message into serializable string */
interface MessageFormatter {
  /** Format message into string */
  public function format(string $format, ...$args): string;
}