<?php

namespace Archaic\Log;

use Archaic\Log\Internal\Instances;

const DATE_FORMAT = 'Y-m-d H:i:s';

function detail(\Exception $e) {
  fprintf(Instances::$stderr, "%s [default] %s\n", date(DATE_FORMAT), $e->getMessage());
  fputs(Instances::$stderr, $e->getTraceAsString());
}

/** log writes message into STDERR. */
function log(string $message) {
  fprintf(Instances::$stderr, "%s [default] %s\n", date(DATE_FORMAT), $message);
}

/** printf formats format and args into message and writes into STDERR. */
function printf(string $format, ...$args) {
  log(sprintf($format, ...$args));
}

/** criticalf formats format and args into message, 
 *  writes it into STDERR and throws and \Exception. */
function criticalf(string $format, ...$args) {
  $message = sprintf($format, ...$args);
  log($message);
  throw new \Exception($message);
}

/** fatalf formats format and args into message, 
 *  writes it into STDERR and exists. */
function fatalf(string $format, ...$args) {
  log(sprintf($format, ...$args));
  exit;
}
