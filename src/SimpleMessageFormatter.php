<?php

namespace Archaic\Log;

class SimpleMessageFormatter implements MessageFormatter {

  private string $dateFormat;
  private string $prefix;

  public function __construct(string $prefix, string $dateFormat = DATE_FORMAT) {
    $this->dateFormat = $dateFormat;
    $this->prefix = $prefix;
  }

  public function format(string $format, ...$args): string {
    $message = sprintf($format, ...$args);
    return sprintf("%s [%s]: %s\n", date($this->dateFormat), $this->prefix, $message);
  }
}