<?php

namespace Archaic\Log;

use Archaic\Log\Internal\Instances;

class StreamLogger {

  private $stream;
  private MessageFormatter $formatter;

  public function __construct(MessageFormatter $formatter, $stream = null) {
    $this->formatter = $formatter;
    $this->stream = $stream ?? Instances::$stderr;
  }

  /** log writes message into this.stream. */
  public function log(string $message) {
    fputs($this->stream, $message);
  }

  /** printf formats format and args into message using this.formatter 
   *  and writes it into this.stream. */
  public function printf(string $format, ...$args) {
    $message = $this->formatter->format($format, ...$args);
    fputs($this->stream, $message);
  }

  /** criticalf formats format and args into message using this.formatter, 
   *  writes it into this.stream and throws and \Exception(message). */
  public function criticalf(string $format, ...$args) {
    $message = $this->formatter->format($format, ...$args);
    fputs($this->stream, $message);
    throw new \Exception($message);
  }

  /** fatalf formats format and args into message using this.formatter, 
   *  writes it into this.stream and exists. */
  public function fatalf(string $format, ...$args) {
    $message = $this->formatter->format($format, ...$args);
    fputs($this->stream, $message);
    exit;
  }
}